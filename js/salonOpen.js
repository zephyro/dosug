
var salonGirl = new Swiper('.salon__girl_slider', {
    init: false,
    slidesPerView: 1,
    spaceBetween: 20,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


jQuery(function($){

    var vh = $(window).width();

    if (vh < 768) {
        salonGirl.init();
    }

    else {

        if($(".salon__girl_slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            salonGirl.destroy(false, true);
        }

        else {

        }
    }

});

$( window ).resize(function() {

    var vh = $(window).width();

    if (vh < 768) {

        if($(".salon__girl_slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
        }
        else {
            console.log('slider stop');
            salonGirl.init();
        }
    }

    else {

        if($(".salon__girl_slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            salonGirl.destroy(false, true);
        }
    }

});