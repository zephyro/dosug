// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());




/* TOOLTIP */

$( document ).ready( function()
{
    var targets = $( '[rel~=tooltip]' ),
        target	= false,
        tooltip = false,
        title	= false;

    targets.bind( 'mouseenter', function()
    {
        target	= $( this );
        tip		= target.attr( 'data-tooltip' );
        tooltip	= $( '<div id="tooltip"></div>' );

        if( !tip || tip == '' )
            return false;

        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
            .html( tip )
            .appendTo( 'body' );

        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 340 );

            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top	 = target.offset().top - tooltip.outerHeight() - 20;

            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );

            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );

            if( pos_top < 0 )
            {
                var pos_top	 = target.offset().top + target.outerHeight();
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );

            tooltip.css( { left: pos_left, top: pos_top } )
                .animate( { top: '+=10', opacity: 1 }, 50 );
        };

        init_tooltip();
        $( window ).resize( init_tooltip );

        var remove_tooltip = function()
        {
            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });

            target.attr( 'title', tip );
        };

        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});

/*  --  */

/*
 VIEWPORT BUG FIX
 iOS viewport scaling bug fix, by @mathias, @cheeaun and @jdalton
 */

( function( doc )
{
    var addEvent = 'addEventListener',
        type	 = 'gesturestart',
        qsa		 = 'querySelectorAll',
        scales	 = [ 1, 1 ],
        meta	 = qsa in doc ? doc[ qsa ]( 'meta[name=viewport]' ) : [];

    function fix()
    {
        meta.content = 'width=device-width,minimum-scale=' + scales[ 0 ] + ',maximum-scale=' + scales[ 1 ];
        doc.removeEventListener( type, fix, true );
    }

    if( ( meta = meta[ meta.length - 1 ] ) && addEvent in doc )
    {
        fix();
        scales = [ .25, 1.6 ];
        doc[ addEvent ]( type, fix, true );
    }

}( document ) );


// Tabs

(function() {

    $('.tabs-nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs-nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('> .tabs-item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());