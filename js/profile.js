// Profile

var feature = new Swiper('.profile__feature', {
    init: false,
    slidesPerView: 1,
    spaceBetween: 40,
    simulateTouch: false,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});

var gallery = new Swiper('.gallery-slider', {
    init: false,
    slidesPerView: 1,
    spaceBetween: 20,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});



jQuery(function($){

    var vh = $(window).width();

    if (vh < 768) {
        feature.init();
        gallery.init();
        //     salonGirl.init();
    }

    else {

        if($(".profile__feature").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            feature.destroy(false, true);
        }

        else {
            console.log('slider stop')
        }

        if($(".gallery-slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            gallery.destroy(false, true);
        }

        else {
            console.log('slider stop')
        }

    }

});

$( window ).resize(function() {

    var vh = $(window).width();

    if (vh < 768) {

        if($(".profile__feature").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
        }
        else {
            console.log('slider stop');
            feature.init();
        }


        if($(".gallery-slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
        }
        else {
            console.log('slider stop');
            gallery.init();
        }

    }

    else {

        if($(".profile__feature").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            feature.destroy(false, true);
        }

        if($(".gallery-slider").hasClass("swiper-container-horizontal")) {
            console.log('slider start');
            gallery.destroy(false, true);
        }
    }

});