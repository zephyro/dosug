
$(".btn-modal").fancybox({
    'padding'    : 0
});

$('.nav-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $('.nav__content').toggleClass('open');
});


$('.account__nav_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.account__nav').toggleClass('open');
});


$('.app__nav_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.app__nav').toggleClass('open');
});



// Показать-скрыть фильтр

$('.btn-filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('close');
    $(this).closest('.filter').find('.filter__full').toggleClass('close');
});


var vacancy = new Swiper('.vacancies-slider', {

    slidesPerView: 4,
    spaceBetween: 40,
    simulateTouch: false,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    breakpoints: {
        1024: {
            slidesPerView: 4,
            spaceBetween: 40
        },

        768: {
            slidesPerView: 1,
            spaceBetween: 20,
            simulateTouch: true
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
            simulateTouch: true
        }
    }
});

// SVG IE11 support
svg4everybody();

// ----- Маска ----------
jQuery(function($){
    $("input[name='phone']").mask("+7(999) 999-9999");
});


// Услуги

$('.price__content > li:first-child').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('ul').toggleClass('open');
});



$('.rank').raty({
    number    : 5,
    score: function() {
        return $(this).attr('data-value');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    },
    path: 'img',
    starHalf : 'star_half.png',
    starOff  : 'star_off.png',
    starOn   : 'star_on.png'
});


$('.review-rank').raty({
    number    : 5,
    score: function() {
        return $(this).attr('data-value');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    },
    path: 'img',
    starHalf : 'star_half_small.png',
    starOff  : 'star_off_small.png',
    starOn   : 'star_on_small.png'
});

var newFace = new Swiper('.newFace-slider', {

    slidesPerView: 8,
    spaceBetween: 20,

    breakpoints: {
        1240: {
            slidesPerView: 6,
            spaceBetween: 20
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 30
        },

        768: {
            slidesPerView: 2,
            spaceBetween: 30
        }
    }
});

var salonGallery = new Swiper('.salon__gallery_slider', {

    slidesPerView: 3,
    spaceBetween: 20,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    breakpoints: {
        1240: {
            slidesPerView: 3
        },
        1024: {
            slidesPerView: 2
        },

        768: {
            slidesPerView: 1
        }
    }
});

var reportsGallery = new Swiper('.reports__gallery_slider', {

    slidesPerView: 4,
    spaceBetween: 10,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    breakpoints: {
        1240: {
            slidesPerView: 4
        },
        1024: {
            slidesPerView: 3
        },

        768: {
            slidesPerView: 1
        }
    }
});

var catalogue = new Swiper('.catalogue__slider', {

    slidesPerView: 1,
    spaceBetween: 20,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});




$('.btn_files input[type="file"]').on('change', function(e) {
    var box = $(this).closest('.file-form');
    var str = $(this).val();

    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    box.find('.file-form-value').text(filename);
});



$('.form-file input[type="file"]').on('change', function(e) {
    var box = $(this).closest('.form-file');
    var str = $(this).val();

    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    box.find('.form-file-value').text(filename);
});

