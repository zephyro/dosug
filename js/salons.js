var salonInfo = new Swiper('.salonBest__slider', {
    slidesPerView: 1,
    spaceBetween: 20,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


<!--- Map -->
ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
        center: [55.74094277730228,37.62643849999995],
        zoom: 16,
        controls: ['smallMapDefaultSet']
    }, {

    });

    myMap.geoObjects
        .add(new ymaps.Placemark([55.73929987507261,37.6345613231129], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.74166039479875,37.63250138658947], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.74174512873827,37.63951804537242], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.73888454917573,37.63783772937922], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))

        .add(new ymaps.Placemark([55.7405675178476,37.62197530419919], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.73915118338601,37.621567608428926], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.738110084112655,37.62508666665646], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.737883494409196,37.628488801903174], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.74179012403114,37.62506520898433], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([55.74112435315682,37.62856280953975], {
            balloonContent: '',
            iconCaption: ''
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#735184'
        }));
}
<!--- -->